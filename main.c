#include <psp2/io/fcntl.h>
#include <psp2/kernel/threadmgr.h>
#include <psp2/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sceRegMgrSetKeyInt(const char*, const char*, int);



int main(int argc, const char *argv[]) {
	sceRegMgrSetKeyInt("/CONFIG/POWER_SAVING", "suspend_interval", 0);
	return 0;
}
